# What is this?

An add-on for World of Warcraft (WoW) Retail.

# Purpose

The add-on helps you to always have a pet out (summoned). Optionally it can summon a random pet (from a configurable pool) every n minutes.

# Usage

/pw a: Toggle automatic summoning of pets  
/pw d: Dismiss current pet and disable auto-summoning  
/pw \<n\>: Interval [minutes] for summoning a new pet. n has to be a number. '0' disables summoning of new pets, though the pet-restore functionality is still active (use '/pw a' to disable it).  
/pw f: Toggle the random-summon pool between Favorites and All Pets. When set to All Pets, the currently active filters of the Pet Journal still apply.  
/pw c: Toggle char-specific favorites list. (Only applies if set to Favorites via '/pw f'.)    
/pw h: Help text  
/pw s: Status report  

Also check the Key Bindings section of your client. You'll find three bindable commands for PetWalker there.

A more detailed description is in work!


